# NetReach documentation

## Components in NetReach

NetReach consists of the following components, which repos are included in the group containing this documentation repository:

- [Linux](https://gitlab.com/distrinet-netreach/linux)

  Contains the infrastructure to process interrupts launched by the Secure World when there is an incoming package.

- OP-TEE OS
    - [OP-TEE OS Core](https://gitlab.com/distrinet-netreach/optee-os)

      Contains the Secure World network driver and the infrastructure to pass interrupts to the Normal World.

    - [OP-TEE examples](https://gitlab.com/distrinet-netreach/optee-examples)

      Contains an application to time ping requests from the Secure World.

- [NetReach specifics](https://gitlab.com/distrinet-netreach/netreach-external)

  Contains the Normal World ethernet driver that works together with the Secure World, part of the infrastructure to receive interrupts coming from the Secure World, configuration files and setup scripts.

- Buildroot build system

    Contains build files to build a full sd-card image for the BD-SL-i.MX6 (the hardware platform of our prototype).

    - [Buildroot Core](https://gitlab.com/distrinet-netreach/buildroot)
    - [i.MX6 Board specifics](https://gitlab.com/distrinet-netreach/buildroot-external-boundary)

### Stable revisions

All repositories in the NetReach group contain a tag `systex-2024`. This tag indicates the revisions used at the time of writing the paper.

### Changes made to base repositories

The Buildroot repositories have been included in the NetReach group to serve as a stable reference. No changes have been made to the repositories compared to those in Mr-TEE.

The other repositories contain a base commit and a commit with the changes made for NetReach. Those commits contain all the changes made while researching the paper.

## Building & running NetReach

The work in NetReach is built on top of the work from Mr-TEE [1]. As such, this page is taken and adjusted from the documentation at [https://distrinet-tacos.github.io/documentation/](https://distrinet-tacos.github.io/documentation/).

### Prerequisites

#### Ubuntu

This guide is based on a clean Ubuntu 24.04 installation. It should be possible to repeat the results on any Linux distribution, as long as the required packages are available.

At least 40-60GiB (estimate) free disk space is required. If the disk is not large enough and the build process is started, the disk will fill, possibly leading to system crashes.

#### Buildroot

We use Buildroot as the build system for the i.MX6.
The required packages need to be installed by executing:

```bash
sudo apt update
sudo apt install sed make binutils build-essential diffutils gcc g++ \
         bash patch gzip bzip2 perl tar cpio unzip rsync file bc findutils \
         wget python3 python-is-python3 libncurses-dev git 
```

Some optional packages are also available, see the [Buildroot Documentation](https://buildroot.org/downloads/manual/manual.html#requirement) for more information.

### Download of repositories

The build system is based on a customized Buildroot. NetReach related changes are maintained in [our fork of Buildroot](https://gitlab.com/distrinet-netreach/buildroot.git). To initialize the build environment, create a project directory and clone the buildroot and the other necessary repos into this directory.

``` bash
PROJECT_DIR=<project-dir>
mkdir -p $PROJECT_DIR
cd $PROJECT_DIR
git clone --depth 1 --branch systex-2024 https://gitlab.com/distrinet-netreach/buildroot.git
git clone --depth 1 --branch systex-2024 https://gitlab.com/distrinet-netreach/buildroot-external-boundary.git
git clone --depth 1 --branch systex-2024 https://gitlab.com/distrinet-netreach/netreach-external.git
```

### Output folder for buildroot

Use the following command to overlay the i.MX6 related configurations, the Board Support Package (BSP), OPTEE configurations and NetReach related configurations on the Buildroot setup. The following command will create a directory named output and place the configurations in this folder.

``` bash
cd $PROJECT_DIR
make BR2_EXTERNAL=$PWD/buildroot-external-boundary/:$PWD/netreach-external/ -C buildroot/ O=$PWD/output imx6q_sabrelite_defconfig
```

### Build OP-TEE, Linux and the i.MX6 BSP

The simplest way to bootstrap is to issue a make command. This will fetch all the components and build Linux, OPTEE and OPTEE applications. The Linux image is bounded to the OPTEE image and an SD card image is created. 
 
``` bash
cd $PROJECT_DIR/output
make BR2_JLEVEL="$(nproc)" all
```

This process will take some time.

### Creating a bootable SD Card

The previous process will create an SD card image in the directory `$PROJECT_DIR/output/images/sdcard.img`. You can use an SD card flash tool such as [balenaEtcher](https://www.balena.io/etcher/) to flash the image on an SD Card.

### Connecting the BD-SL-i.MX6 Serial

The BD-SL-i.MX6 board has two UART connections. The UART connection labled 'Console' will be connected to OPTEE while the other UART will be connected to the Linux running in the Normal world. Once the UART cables are connected and their interfaces are attached to separate console windows. Power up the BD-SL-i.MX6 board and you will see boot logs for OPTEE on the UART labeled console and the Linux logs on the other UART. You can now log in to linux and look around or start an example client application available onboard.

It might be that only one serial connection is used to output both the normal and the secure world consoles. To change this, during the first moments of the boot process, press any key to stop the autoboot. Enter the following in the terminal that appears.

``` txt
setenv console ttymxc0
saveenv
reboot
```

The device should now use both serial connections.

### Secure Ethernet

To get everything up and running, execute the following command in the Linux command prompt of the i.MX6 to enable the necessary kernel modules and configure the ethernet peripheral. It does the following:

1. Load the kernel module responsible for receiving and passing on the interrupts to the network driver.
1. Load the network driver that interacts with the driver in the Secure World.
1. Set the ip address of the Normal World network.

```bash
setup-network.sh
```

At this point both the Normal World network and Secure World network should be active. The normal `ping` command can be used to measure the latency from the Normal World to a remote device (crossing the Secure World as explained in the paper). The command `optee_example_ping_test` (implemented [here](https://gitlab.com/distrinet-netreach/optee-examples/-/blob/main/ping_test/host/main.c?ref_type=heads)) can be used to measure the latency from the Secure World to a remote device.

## References

[1] Tom Van Eyck, Hamdi Trimech, Sam Michiels, Danny Hughes, Majid Salehi, Hassan Janjuaa, and Thanh-Liem Ta. 2023. Mr-TEE: Practical Trusted Execution of Mixed-Criticality Code. In Proceedings of the 24th International Middleware Conference: Industrial Track (Middleware '23). Association for Computing Machinery, New York, NY, USA, 22–28. https://doi.org/10.1145/3626562.3626831
